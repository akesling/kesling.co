+++
title = ""
template = "index.html"
page_template = "index.html"
+++

Hi, <a href="/about">I'm Alex</a> and this is an attempt at a persistent
presence on the web.

Check out <a href="/words">words I've written</a> here, or
<a href="/projects"> projects I've worked on</a> over the years.

You can <a href="mailto:words@kesling.co">contact me directly</a> or find me on <a href="https://twitter.com/alexkesling">Twitter</a>
and <a href="https://www.linkedin.com/in/alexkesling">LinkedIn</a>.
